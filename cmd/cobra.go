package cmd

import (
	"errors"
	"fms/cmd/api"
	"fms/cmd/config"
	"fms/cmd/migrate"
	"fms/cmd/version"
	"fms/global"
	"fms/tools"
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

var rootCmd = &cobra.Command{
	Use:          "fms",
	Short:        "fms",
	SilenceUsage: true,
	Long:         `fms`,
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			tip()
			return errors.New(tools.Red("requires at least one arg"))
		}
		return nil
	},
	PersistentPreRunE: func(*cobra.Command, []string) error { return nil },
	Run: func(cmd *cobra.Command, args []string) {
		tip()
	},
}

func tip() {
	usageStr := "欢迎使用 " + tools.Green("fms "+global.Version) + " 可以使用 " + tools.Red("-h") + " 查看命令"
	usageStr1 := "也可以参考 http://doc.zhangwj.com/fms-site/guide/ksks.html 里边的【启动】章节"
	fmt.Printf("%s\n", usageStr)
	fmt.Printf("%s\n", usageStr1)
}

func init() {
	rootCmd.AddCommand(api.StartCmd)
	rootCmd.AddCommand(migrate.StartCmd)
	rootCmd.AddCommand(version.StartCmd)
	rootCmd.AddCommand(config.StartCmd)
}

//Execute : apply commands
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(-1)
	}
}
