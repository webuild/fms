package main

import (
	"fms/global"
	mycasbin "fms/pkg/casbin"
	"fms/pkg/logger"
	"fms/router"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"log"
)

func main() {
	var err error
	global.Eloquent, err = gorm.Open("mysql", "root:123456@tcp/inmg?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	mycasbin.Setup()
	logger.Setup()
	global.GinEngine = gin.Default()
	router.InitRouter()
	log.Fatal(global.GinEngine.Run(":8000"))
}
