package router

import (
	"fms/apis/fileinfo"
	"fms/middleware"
	jwt "fms/pkg/jwtauth"
	"github.com/gin-gonic/gin"
)

/**
 * @author anyang
 * Email: 1300378587@qq.com
 * Created Date:2020-09-20 18:17
 */
// 需认证的路由代码
func registerFileInfoRouter(v1 *gin.RouterGroup, authMiddleware *jwt.GinJWTMiddleware) {

	r := v1.Group("/fileinfo").Use(authMiddleware.MiddlewareFunc()).Use(middleware.AuthCheckRole())
	{
		r.GET("/:fileId", fileinfo.GetFileInfo)
		r.POST("", fileinfo.InsertFileInfo)
		r.PUT("", fileinfo.UpdateFileInfo)
		r.DELETE("/:fileId", fileinfo.DeleteFileInfo)
	}

	l := v1.Group("").Use(authMiddleware.MiddlewareFunc()).Use(middleware.AuthCheckRole())
	{
		l.GET("/fileinfoList", fileinfo.GetFileInfoList)
	}

}
