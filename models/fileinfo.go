package models

import (
	"errors"
	orm "fms/global"
	"fms/pkg"
	"fms/tools"
	"fmt"
)

type FileInfo struct {
	FileId    int    `json:"fileId" gorm:"type:int(11);primary_key"` // 主键
	Name      string `json:"name" gorm:"type:varchar(255);"`         // 名称
	Md5       string `json:"md5" gorm:"type:varchar(255);"`          //
	Domain    string `json:"domain" gorm:"type:varchar(255);"`       //
	Url       string `json:"url" gorm:"type:varchar(255);"`          //
	Scene     string `json:"scene" gorm:"type:varchar(255);"`        //
	Src       string `json:"src" gorm:"type:varchar(255);"`          //
	Size      string `json:"size" gorm:"type:bigint(20);"`           //
	Tail      string `json:"tail" gorm:"type:varchar(50);"`          // 文件类型
	Remark    string `json:"remark" gorm:"type:varchar(255);"`       // 备注
	DataScope string `json:"dataScope" gorm:"-"`
	Params    string `json:"params"  gorm:"-"`
	CreateBy  string `json:"createBy" gorm:"size:128;"` //
	UpdateBy  string `json:"updateBy" gorm:"size:128;"` //
	BaseModel
}

func (FileInfo) TableName() string {
	return "file_info"
}

// 创建FileInfo
func (e *FileInfo) Create() (FileInfo, error) {
	var doc FileInfo
	result := orm.Eloquent.Table(e.TableName()).Create(&e)
	if result.Error != nil {
		err := result.Error
		return doc, err
	}
	doc = *e
	return doc, nil
}

// 获取FileInfo
func (e *FileInfo) Get() (FileInfo, error) {
	var doc FileInfo
	table := orm.Eloquent.Table(e.TableName())

	if e.FileId != 0 {
		table = table.Where("file_id = ?", e.FileId)
	}

	if e.Name != "" {
		table = table.Where("name like ?", "%"+e.Name+"%")
	}

	if e.Tail != "" {
		table = table.Where("tail like ?", "%"+e.Tail+"%")
	}

	if e.Remark != "" {
		table = table.Where("remark like ?", "%"+e.Remark+"%")
	}

	if err := table.First(&doc).Error; err != nil {
		return doc, err
	}
	return doc, nil
}

// 获取FileInfo带分页
func (e *FileInfo) GetPage(pageSize int, pageIndex int) ([]FileInfo, int, error) {
	var doc []FileInfo

	table := orm.Eloquent.Select("*").Table(e.TableName())

	if e.Name != "" {
		table = table.Where("name like ?", "%"+e.Name+"%")
	}

	if e.Tail != "" {
		table = table.Where("tail like ?", "%"+e.Tail+"%")
	}

	if e.Remark != "" {
		table = table.Where("remark like ?", "%"+e.Remark+"%")
	}

	// 数据权限控制(如果不需要数据权限请将此处去掉)
	dataPermission := new(DataPermission)
	dataPermission.UserId, _ = tools.StringToInt(e.DataScope)
	table, err := dataPermission.GetDataScope(e.TableName(), table)
	if err != nil {
		return nil, 0, err
	}
	var count int

	if err := table.Offset((pageIndex - 1) * pageSize).Limit(pageSize).Find(&doc).Error; err != nil {
		return nil, 0, err
	}
	table.Where("`deleted_at` IS NULL").Count(&count)
	return doc, count, nil
}

// 更新FileInfo
func (e *FileInfo) Update(id int) (update FileInfo, err error) {
	if err = orm.Eloquent.Table(e.TableName()).Where("file_id = ?", id).First(&update).Error; err != nil {
		return
	}

	//参数1:是要修改的数据
	//参数2:是修改的数据
	if err = orm.Eloquent.Table(e.TableName()).Model(&update).Updates(&e).Error; err != nil {
		return
	}
	return
}

// 删除FileInfo
func (e *FileInfo) Delete(id int) (success bool, err error) {
	if err = orm.Eloquent.Table(e.TableName()).Where("file_id = ?", id).Delete(&FileInfo{}).Error; err != nil {
		success = false
		return
	}
	success = true
	return
}

//批量删除
func (e *FileInfo) BatchDelete(id []int) (Result bool, err error) {
	if err = orm.Eloquent.Table(e.TableName()).Where("file_id in (?)", id).Delete(&FileInfo{}).Error; err != nil {
		return
	}
	Result = true
	return
}

func (e *FileInfo) BatchClean(md5s []string) (Result bool, err error) {
	dest := fmt.Sprintf("%s/%s/delete?md5=")
	Result = true
	for _, md5 := range md5s {
		_, err1 := pkg.Delete(dest + md5)
		if err1 != nil {
			Result = false
			err = errors.New(fmt.Sprint(err, err1))
		}
	}
	return
}
