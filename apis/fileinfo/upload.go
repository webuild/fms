package fileinfo

import (
	"encoding/base64"
	"errors"
	"fms/pkg/utils"
	"fms/tools/app"
	"fmt"
	"github.com/astaxie/beego/httplib"
	"github.com/eventials/go-tus"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"io/ioutil"
	"os"
)

func UploadFile(c *gin.Context) {
	tag, _ := c.GetPostForm("type")
	urlPerfix := fmt.Sprintf("http://%s/", c.Request.Host)
	if tag == "" {
		app.Error(c, 200, errors.New(""), "缺少标识")
		return
	} else {
		switch tag {
		case "1": // 单图
			files, err := c.FormFile("file")
			if err != nil {
				app.Error(c, 200, errors.New(""), "图片不能为空")
				return
			}
			// 上传文件至指定目录
			guid := uuid.New().String()

			singleFile := "static/uploadfile/" + guid + utils.GetExt(files.Filename)
			_ = c.SaveUploadedFile(files, singleFile)
			app.OK(c, urlPerfix+singleFile, "上传成功")
			return
		case "2": // 多图
			files := c.Request.MultipartForm.File["file"]
			multipartFile := make([]string, len(files))
			for _, f := range files {
				guid := uuid.New().String()
				multipartFileName := "static/uploadfile/" + guid + utils.GetExt(f.Filename)
				_ = c.SaveUploadedFile(f, multipartFileName)
				multipartFile = append(multipartFile, urlPerfix+multipartFileName)
			}
			app.OK(c, multipartFile, "上传成功")
			return
		case "3": // base64
			files, _ := c.GetPostForm("file")
			ddd, _ := base64.StdEncoding.DecodeString(files)
			guid := uuid.New().String()
			_ = ioutil.WriteFile("static/uploadfile/"+guid+".jpg", ddd, 0666)
			app.OK(c, urlPerfix+"static/uploadfile/"+guid+".jpg", "上传成功")
		}
	}
}

func uploadServer() {
	var obj interface{}
	req := httplib.Post("http://10.1.5.9:8080/group1/upload")
	req.PostFile("file", "filename") //注意不是全路径

	req.Param("output", "json")
	req.Param("scene", "")
	req.Param("path", "")
	req.ToJSON(&obj)
	fmt.Print(obj)

}

func uploadContinue() {
	f, err := os.Open("100m")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	// create the tus client.
	client, err := tus.NewClient("http://10.1.5.9:8080/big/upload/", nil)
	fmt.Println(err)
	// create an upload from a file.
	upload, err := tus.NewUploadFromFile(f)
	fmt.Println(err)
	// create the uploader.
	uploader, err := client.CreateUpload(upload)
	fmt.Println(err)
	// start the uploading process.
	fmt.Println(uploader.Upload())

}

// import (
//    "os"
//    "fmt"
//    "github.com/eventials/go-tus"
//)
//
//func main() {
//    f, err := os.Open("100m")
//    if err != nil {
//        panic(err)
//    }
//    defer f.Close()
//    // create the tus client.
//    client, err := tus.NewClient("http://10.1.5.9:8080/big/upload/", nil)
//    fmt.Println(err)
//    // create an upload from a file.
//    upload, err := tus.NewUploadFromFile(f)
//    fmt.Println(err)
//    // create the uploader.
//    uploader, err := client.CreateUpload(upload)
//    fmt.Println(err)
//    // start the uploading process.
//   fmt.Println( uploader.Upload())
//
//}

// {
//  "data": {
//    "domain": "http://127.0.0.1:8080",
//    "md5": "c344a8ab8effcf65761623156e4ecccd",
//    "mtime": 1600516445,
//    "path": "/group1/default/20200919/19/54/7/IMG_20200907_224939(1).jpg",
//    "retcode": 0,
//    "retmsg": "",
//    "scene": "default",
//    "scenes": "default",
//    "size": 1515500,
//    "src": "/group1/default/20200919/19/54/7/IMG_20200907_224939(1).jpg",
//    "url": "http://127.0.0.1:8080/group1/default/20200919/19/54/7/IMG_20200907_224939(1).jpg"
//  },
//  "message": "",
//  "status": "ok"
//}

//package main
//
//import (
//    "fmt"
//    "github.com/astaxie/beego/httplib"
//)
//
//func main()  {
//    var obj interface{}
//    req:=httplib.Post("http://10.1.5.9:8080/group1/upload")
//    req.PostFile("file","filename")//注意不是全路径
//    req.Param("output","json")
//    req.Param("scene","")
//    req.Param("path","")
//    req.ToJSON(&obj)
//    fmt.Print(obj)
//}
