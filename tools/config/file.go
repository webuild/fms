package config

import "github.com/spf13/viper"

/**
 * @author anyang
 * Email: 1300378587@qq.com
 * Created Date:2020-09-22 20:31
 */

type File struct {
	Base  string
	Group string
}

func InitFile(cfg *viper.Viper) *File {
	return &File{
		Base:  cfg.GetString("base"),
		Group: cfg.GetString("group"),
	}
}

var FileConfig = new(File)
